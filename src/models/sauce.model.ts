
export interface Sauce {
	sauceType: string;
	links: Array<any>;
	articleID: number;
}
