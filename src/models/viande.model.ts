
export interface Viande {
	type: string;
	links: Array<any>;
	articleID: number;
}
