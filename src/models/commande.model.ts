import { Menu } from './menu.model';

export interface Commande {
	cdeID: number;
	menus: Array<Menu>;
	typeCommande: string;
	etatCommande: string;
	prixTotal: number;
}
