import { Menu } from './menu.model';

export interface Order {
	cdeID: number;
	menus: Array<Menu>;
	typeCommande: string;
	etatCommande: string;
	prixTotal: number;
}
