
export interface Frite {
	type: string;
	links: Array<any>;
	articleID: number;
}
