
export interface Boisson {
	boissonCapacity: string;
	name: string;
	prixBoisson: number;
	avecMenu: boolean;
	links: Array<any>;
	articleID: number;
}
