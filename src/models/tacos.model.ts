
export interface Tacos {
	prix: number;
	size: string;
	meatNumber: number;
	links: Array<any>;
	articleID: number;
}
