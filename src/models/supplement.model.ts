
export interface Supplement {
	type: string;
	supplementPrice: number;
	links: Array<any>;
	articleID: number;
}
