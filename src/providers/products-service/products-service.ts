import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import { Frite } from '../../models/frite.model';
import { Sauce } from '../../models/sauce.model';
import { Supplement } from '../../models/supplement.model';
import { Tacos } from '../../models/tacos.model';
import { Viande } from '../../models/viande.model';

/*
  Generated class for the ProductsServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProductsServiceProvider {
	private base_url = 'http://tacos-time29.appspot.com';
	constructor(public http: HttpClient) {}
	getTacos(){
		return this.http.get<Tacos[]>(this.base_url + '/article/tacos');
	}
	getViandes() {
		return this.http.get<Viande[]>(this.base_url + '/article/viandes');
	}
	getSauces() {
		return this.http.get<Sauce[]>(this.base_url + '/article/sauces');
	}
	getSupplements() {
		return this.http.get<Supplement[]>(this.base_url + '/article/supplements');
	}
	/*getBoissons() {
		return this.http.get<Boisson[]>(this.base_url + '/article/boissons');
	}*/
	getFrites() {
		return this.http.get<Frite[]>(this.base_url + '/article/frites');
	}
	getAllProducts() {
		return Observable.forkJoin(
			this.http.get<Tacos[]>(this.base_url + '/article/tacos'),
			this.http.get<Viande[]>(this.base_url + '/article/viandes'),
			this.http.get<Sauce[]>(this.base_url + '/article/sauces'),
			this.http.get<Supplement[]>(this.base_url + '/article/supplements'),
			// this.http.get<Boisson[]>(this.base_url + '/article/boissons'),
			this.http.get<Frite[]>(this.base_url + '/article/frites')
		);
	}
}
