import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Commande } from '../../models/commande.model';

/*
  Generated class for the OrderServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OrderServiceProvider {
	private base_url = 'http://tacos-time29.appspot.com';
	
	constructor(public http: HttpClient) {}

	addNewOrder(commande) {
		const headers = new HttpHeaders({'Content-type': 'application/json; charset=utf-8'});
		const options = {headers : headers};
		return this.http.post<Commande>(this.base_url + '/commande/new-order', JSON.stringify(commande), options);
	}

	updateOrder(idCmd, newState) {
		const headers = new HttpHeaders({'Content-type': 'application/json; charset=utf-8'});
		const options = {headers : headers};
		return this.http.put(this.base_url + '/commande/change-state/' + idCmd + '/' + newState, {}, options);
	}

	getOrdersByState(state: string) {
		return this.http.get<Commande>(this.base_url + '/commande/state/' + state);
	}
}
