import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavController, Slides, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { OrderServiceProvider } from '../../providers/order-service/order-service';
import { HomePage } from '../home/home';

/**
 * Generated class for the NewOrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-order',
  templateUrl: 'new-order.html',
})
export class NewOrderPage implements OnInit {
	@ViewChild(Slides) slides: Slides;
	
	private tacos: any;
	private viandes: any;
	private sauces: any;
	private supplements: any;
	private frites: any;
	
	private tailleBoisson = '33cl';
	private tailleTacos: string;
	private typeFrites: number;
	private typeCmd: string;
	
	private lstViandes: Object;
	private lstSauces: Object;
	private lstSupplements: Object;
	
	private currentCommande: Object;
	
	constructor(public navCtrl: NavController, public navParams: NavParams, private orderService: OrderServiceProvider,
				private storage: Storage, public loadingController: LoadingController, private alertCtrl: AlertController) {
					this.lstViandes = new Object();
					this.lstSauces = new Object();
					this.lstSupplements = new Object();
					this.currentCommande = new Object();
				}
	ngOnInit() {
		const loading = this.loadingController.create({
		  content: 'Chargement des articles en cours...'
		});
		loading.present().then(() => {
			this.storage.get('tacos').then((data) => {
				this.tacos = data;
				//console.log(data);
			});
			this.storage.get('viandes').then((data) => {
				this.viandes = data;
				//console.log(data);
			});
			this.storage.get('sauces').then((data) => {
				this.sauces = data;
				//console.log(data);
			});
			this.storage.get('supplements').then((data) => {
				this.supplements = data;
				//console.log(data);
			});
			this.storage.get('frites').then((data) => {
				this.frites = data;
				//console.log(data);
			});
			loading.dismiss();
		});
	}
	
	slideChanged() {
		if(this.slides.isBeginning()) {
			this.slides.lockSwipeToNext(typeof this.tailleTacos === "undefined");
		} else {
			switch(this.slides.getActiveIndex()) {
				case 1:
					this.slides.lockSwipeToNext(this.disableSwipFromViandeToSauce());
					break;
				case 2:
					this.slides.lockSwipeToNext(this.disableSwipFromSauceToSuppl());
					break;
				case 3:
					this.slides.lockSwipeToNext(this.disableSwipFromSupplToAccom());
					break;
				case 4:
					this.slides.lockSwipeToNext(this.disableSwipFromAccomToBoisson());
					break;
				case 5:
					this.slides.lockSwipeToNext(this.disableSwipFromBoissonToRecap());
					break;
				default:
					this.slides.lockSwipeToNext(false);
			}
		}
	}
	
	disableSwipFromViandeToSauce(): boolean {
		if(Object.keys(this.lstViandes).length === parseInt(this.tailleTacos)) {
			return false;
		} else {
			return true;
		}
	}
	
	disableSwipFromSauceToSuppl(): boolean {
		if(Object.keys(this.lstSauces).length <= 2) {
			return false;
		} else {
			return true;
		}
	}
	
	disableSwipFromSupplToAccom(): boolean {
		if(Object.keys(this.lstSupplements).length <= 3) {
			return false;
		} else {
			return true;
		}
	}
	
	disableSwipFromAccomToBoisson(): boolean {
		if(typeof this.typeFrites === "undefined") {
			return true;
		} else {
			return false;
		}
	}
	
	disableSwipFromBoissonToRecap(): boolean {
		if(typeof this.tailleBoisson === "undefined") {
			return true;
		} else {
			this.setCurrentCommande();
			return false;
		}
	}
	
	tacosChange(value) {
		this.tailleTacos = value;
		this.slides.lockSwipeToNext(false);
		this.slides.slideNext(600, false);
	}
	
	viandesChange(value) {
		//console.log(value);
		if(value.checked === true) {
			this.lstViandes[value._item._label.text] = value._item._label.text;
			this.slides.lockSwipeToNext(this.disableSwipFromViandeToSauce());
			this.slides.slideNext(600, false);
		} else {
			delete this.lstViandes[value._item._label.text];
			this.slides.lockSwipeToNext(this.disableSwipFromViandeToSauce());
			this.slides.slideNext(600, false);
		}
	}
	
	saucesChange(value) {
		//console.log(value);
		if(value.checked === true) {
			this.lstSauces[value._item._label.text] = value._item._label.text;
			this.slides.lockSwipeToNext(this.disableSwipFromSauceToSuppl());
			this.slides.slideNext(600, false);
		} else {
			delete this.lstSauces[value._item._label.text];
			this.slides.lockSwipeToNext(this.disableSwipFromSauceToSuppl());
			this.slides.slideNext(600, false);
		}
	}
	
	supplementsChange(value) {
		//console.log(value);
		if(value.checked === true) {
			this.lstSupplements[value._item._label.text] = value._item._label.text;
			this.slides.lockSwipeToNext(this.disableSwipFromSupplToAccom());
			this.slides.slideNext(600, false);
		} else {
			delete this.lstSupplements[value._item._label.text];
			this.slides.lockSwipeToNext(this.disableSwipFromSupplToAccom());
			this.slides.slideNext(600, false);
		}
	}
	
	fritesChange(value) {
		//console.log(value);
		this.typeFrites = value;
		this.slides.lockSwipeToNext(this.disableSwipFromAccomToBoisson());
		this.slides.slideNext(600, false);
	}
	
	boissonsChange(value) {
		//console.log(value);
		this.tailleBoisson = value;
		this.slides.lockSwipeToNext(this.disableSwipFromBoissonToRecap());
		this.slides.slideNext(600, false);
	}
	
	placeChange(value) {
		//console.log("Place :");
		//console.log(value);
		this.typeCmd = value;
	}
	
	setCurrentCommande() {
		//console.log(this.tacos);
		this.currentCommande["tailleTacos"] = this.tacos[parseInt(this.tailleTacos)-1].size;
		this.currentCommande["viande"] = this.slugify(this.lstViandes);
		this.currentCommande["supplement"] = this.slugify(this.lstSupplements) || 'SANS';
		this.currentCommande["sauce"] = this.slugify(this.lstSauces) || 'SANS';
		this.currentCommande["frite"] = this.resolveTypeFrite(this.typeFrites) || 'SANS';
		this.currentCommande["boisson"] = this.tailleBoisson || 'SANS';
	}
	
	resolveTypeFrite(id: number) {
		//console.log("ID "+id);
		if(id == 14) {
			return "POTATOES";
		} else if (id == 13) {
			return "FRITES";
		} else {
			return "SANS";
		}
	}
	
	slugify(objet: Object) {
		let res = "";
		Object.keys(objet).forEach(function(val) { res += val + " / "; });
		return res.substring(0, res.length - 2);;
	}
	
	// Méthode appelée quand le client valide sa commande
	valider() {
		let articles = [];
		
		let t = this.tacos.find( (el) => {return el.articleID == this.tailleTacos});
		//console.log(t);
		articles.push({"tacos": {
				"prix":t.prix,
				"size":t.size,
				"meatNumber":t.meatNumber,
				"articleID":t.articleID
			}
		});
		for(let i = 0; i < this.viandes.length; i++) {
			if(this.lstViandes.hasOwnProperty(this.viandes[i].type)) {
				let vmenu = {
					"meat": {
						"type" : "KEBAB",
						"articleID" : this.viandes[i].articleID
					}
				};
				articles.push(vmenu);
			}
		}
		for(var i = 0; i < this.supplements.length; i++) {
			if(this.lstSupplements.hasOwnProperty(this.supplements[i].type)) {
				var supmenu = {
					"supplement" : {
						"type" : this.supplements[i].type,
						"supplementPrice" : this.supplements[i].supplementPrice,
						"articleID" : this.supplements[i].articleID
					}
				};
				articles.push(supmenu);
			}
		}
		for(let i = 0; i < this.sauces.length; i++) {
			if(this.lstSauces.hasOwnProperty(this.sauces[i].sauceType)) {
				let saumenu = {
					"sauce": {
						"sauceType": this.sauces[i].sauceType,
						"articleID": this.sauces[i].articleID
					}
				};
				articles.push(saumenu);
			}
		}
		
		if(this.typeFrites == 13) {
			//console.log("YAAAAAAAAAP");
			let fries = {
				"fries": {
					"type": "SIMPLE",
					"articleID": 13
				}
			};
			articles.push(fries);
			
		} else if (this.typeFrites == 14) {
			//console.log("YUUUUUUUUUP");
			let fries = {
				"fries": {
					"type": "POTATOES",
					"articleID": 14
				}
			};
			articles.push(fries);
		} else {}

		if (this.tailleBoisson === '33cl') {
			let bmenu = {
					"boisson": {
						"boissonCapacity" : "CL33",
						"name" : "Coca-Cola",
						"prixBoisson" : 1,
						"avecMenu" : true,
						"articleID" : 36
					}
				};
				articles.push(bmenu);
		} else if (this.tailleBoisson === '50cl') {
			let bmenu = {
					"boisson": {
						"boissonCapacity" : "CL50",
						"name" : "Coca-Cola",
						"prixBoisson" : 1,
						"avecMenu" : true,
						"articleID" : 38
					}
				};
				articles.push(bmenu);
		} else {}
		
		let menu = {
			"menuID": null,
			"articles": articles,
			"priceMenu": 1
		};
		let commande = {"Commande": {
			"cdeID": null,
			"menus": [menu],
			"typeCommande": this.typeCmd || "SUR_PLACE",
			"etatCommande": "EN_ATTENTE",
			"prixTotal": 1
		}};
		
		/*
		console.log("==========CMD TO SEND==========");
		console.log(commande);
		console.log("===============================");
		*/
		
		this.orderService.addNewOrder(commande)
			.subscribe(
				data => {
					// console.log(data);
					let alert = this.alertCtrl.create({
						title: 'Commande validée !',
						message: 'Nous vous appellerons quand elle sera prête.',
						buttons: [
						  {
							text: 'OK',
							handler: () => {
							  //console.log('OK clicked');
							  this.navCtrl.pop();
							}
						  }
						]
					  });
					  alert.present();
				},
				error => {
					//console.log(error);
					this.errorAlert(error);
			});
	}
	
	errorAlert(error) {
		const alert = this.alertCtrl.create({
			title: 'Erreur !',
			subTitle: error.status,
			message: error.message,
			buttons: [
				{
					text:'OK',
					handler: () => {
							  //console.log('OK clicked');
							  this.navCtrl.setRoot(HomePage);
							}
				}
			]
		});
		alert.present();
	}
}
