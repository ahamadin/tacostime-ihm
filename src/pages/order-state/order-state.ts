import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { OrderServiceProvider } from '../../providers/order-service/order-service';
import { HomePage } from '../home/home';

/**
 * Generated class for the OrderStatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order-state',
  templateUrl: 'order-state.html',
})
export class OrderStatePage {
	
	private currState: string;
	private idCmd: number;
	
	constructor(public navCtrl: NavController, public params: NavParams, private alertCtrl: AlertController,
				private orderService: OrderServiceProvider) {
					this.currState = params.get('currState');
					this.idCmd = params.get('idCmd');
				}
	
	resolveState(state: string): string {
		if(state === 'EN_ATTENTE') {
			return 'attente';
		} else if (state === 'EN_PREPARATION') {
			return 'preparation';
		} else if (state === 'TERMINEE') {
			return 'terminee';
		} else if (state === 'EN_LIVRAISON') {
			return 'livraison';
		} else {
			return 'livree';
		}
	}
	
	
	onChangeState(newState: string) {
		this.orderService.updateOrder(this.idCmd, this.resolveState(newState))
		.subscribe(
				data => {
					//console.log(data);
				},
				error => {
					//console.log(error);
					this.errorAlert(error);
				});
	}
	
	errorAlert(error) {
		const alert = this.alertCtrl.create({
			title: 'Erreur !',
			subTitle: error.status,
			message: error.message,
			buttons: [
				{
					text:'OK',
					handler: () => {
							  //console.log('OK clicked');
							  this.navCtrl.setRoot(HomePage);
							}
				}
			]
		});
		alert.present();
	}
	closeModal() {
        this.navCtrl.pop();
    }

}
