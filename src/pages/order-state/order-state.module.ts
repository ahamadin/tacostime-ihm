import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderStatePage } from './order-state';

@NgModule({
  declarations: [
    OrderStatePage,
  ],
  imports: [
    IonicPageModule.forChild(OrderStatePage),
  ],
})
export class OrderStatePageModule {}
