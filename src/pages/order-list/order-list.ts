import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, AlertController } from 'ionic-angular';
import { Observable } from 'rxjs/Rx';
import { OrderServiceProvider } from '../../providers/order-service/order-service';
import { OrderStatePage } from '../../pages/order-state/order-state';
import { HomePage } from '../home/home';

/**
 * Generated class for the OrderListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order-list',
  templateUrl: 'order-list.html',
})
export class OrderListPage {
	
	pollingData: any;
	private selectedOrders: any;
	private filterStateValue = 'EN_ATTENTE';	

	constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
				private orderService: OrderServiceProvider, public loadingController: LoadingController,
				private alertCtrl: AlertController) {}
				
	ngOnInit() {
		const loading = this.loadingController.create({
		  content: 'Chargement des commandes en cours...'
		});
		loading.present().then(() => {
			this.orderService.getOrdersByState(this.resolveState(this.filterStateValue))
			.subscribe(
				data => {
					this.selectedOrders = data;
					loading.dismiss();
					this.polling();
					//console.log(data);// see console you get output every 5 sec
				},
				error => {
					//console.log(error);
					if(error.status === 404) {
						loading.dismiss();
						this.selectedOrders = [];
					} else {
						this.errorAlert(error);
					}
				});
		});
	}
	polling() {
		this.pollingData = Observable.interval(3000)
			.switchMap(() => this.orderService.getOrdersByState(this.resolveState(this.filterStateValue)))
			.subscribe(
				data => {
					this.selectedOrders = data;				
					//console.log(data);// see console you get output every 5 sec
				},
				error => {
					//console.log(error);
					if(error.status === 404) {
						this.selectedOrders = [];
					} else {
						this.errorAlert(error);
					}
				});
	}
	
	onChangeStateF(value) {
		const loading = this.loadingController.create({
		  content: 'Chargement en cours...'
		});
		loading.present().then(() => {
			this.orderService.getOrdersByState(this.resolveState(value))
				.subscribe(
				data => {
					this.selectedOrders = null;
					this.selectedOrders = data;
					loading.dismiss();
				},
				error => {
					//console.log(error);
					loading.dismiss();
					if(error.status === 404) {
						this.selectedOrders = [];
					} else {
						this.errorAlert(error);
					}
				});
		});
		
	}
	
	update(idCmd: number, currState: string) {
		//console.log("Update => ID : "+idCmd+" CurState : "+currState);
		const orderStateModal = this.modalCtrl.create(OrderStatePage, {idCmd: idCmd, currState: currState});
		orderStateModal.onDidDismiss(() => {
           this.onChangeStateF(currState);
		});
	    orderStateModal.present();
	}
	
	viandesStringify(tab: any): string {
		let res = "";
		for(let i = 0; i < tab.length; i++) {
			if(tab[i].meat)
				res += tab[i].meat.type + " / ";
		}
		return res.substring(0, res.length - 2);
	}
	
	supplementsStringify(tab: any): string {
		let res = "";
		for(let i = 0; i < tab.length; i++) {
			if(tab[i].supplement)
				res += tab[i].supplement.type + " / ";
		}
		return res.substring(0, res.length - 2);
	}
	
	saucesStringify(tab: any): string {
		let res = "";
		for(let i = 0; i < tab.length; i++) {
			if(tab[i].sauce)
				res += tab[i].sauce.sauceType + " / ";
		}
		return res.substring(0, res.length - 2);
	}
	
	friteType(tab: any): string {
		let res;
		for(let i = 0; i < tab.length; i++) {
			if(tab[i].fries) {
				res = this.resolveFrite(tab[i].fries.type);
			}
		}
		return res || 'SANS FRITES';
	}
	
	tailleBoisson(tab: any): string {
		let res = '';
		for(let i = 0; i < tab.length; i++) {
			if(tab[i].boisson) {
				res = tab[i].boisson.boissonCapacity;
			} else {
				res = 'SANS BOISSON';
			}
		}
		return res;
	}
	
	resolveState(state: string): string {
		if(state === 'EN_ATTENTE') {
			return 'attente';
		} else if (state === 'EN_PREPARATION') {
			return 'preparation';
		} else if (state === 'TERMINEE') {
			return 'terminee';
		} else if (state === 'EN_LIVRAISON') {
			return 'livraison';
		} else {
			return 'livree';
		}
	}
	
	resolveFrite(libelle: string): string {
		if(libelle === "POTATOES") {
			return 'POTATOES';
		} else {
			return 'FRITES';
		}
	}
	
	errorAlert(error) {
		const alert = this.alertCtrl.create({
			title: 'Erreur !',
			subTitle: error.status,
			message: error.message,
			buttons: [
				{
					text:'OK',
					handler: () => {
							  this.navCtrl.setRoot(HomePage);
							}
				}
			]
		});
		alert.present();
	}
	ngOnDestroy() {
		if(this.pollingData)
			this.pollingData.unsubscribe();
	}
}
