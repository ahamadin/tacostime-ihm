import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { NewOrderPage } from '../new-order/new-order';
import { OrderListPage } from '../order-list/order-list';
import { ProductsServiceProvider } from '../../providers/products-service/products-service';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
	constructor(public navCtrl: NavController, public loadingController: LoadingController,
				private storage: Storage, private productsService: ProductsServiceProvider,
				private toastController: ToastController, private alertController: AlertController) {}
	ngOnInit() {
		this.loadingProducts();
		/*this.productsService.getBoissons().subscribe(
				data => {
					console.log(data);
				},
				error => {
					this.errorAlert(error);
				});
				*/
		
	}
	loadingProducts() {
		const loading = this.loadingController.create({
		  content: 'Chargement des articles en cours...'
		});
		loading.present().then(() => {
			this.productsService.getAllProducts().subscribe(
				data => {
					this.storage.clear().then(() => {
						this.storage.set('tacos', data[0]);
						this.storage.set('viandes', data[1]);
						this.storage.set('sauces', data[2]);
						this.storage.set('supplements', data[3]);
						this.storage.set('frites', data[4]);
						loading.dismiss();
						this.readyToast();
					});
				},
				error => {
					loading.dismiss();
					this.errorAlert(error);
				});
		});
	}
	readyToast() {
		const toast = this.toastController.create({
			message: 'Bonjour, vous pouvez commander votre Tacos !',
			showCloseButton: true,
			position: 'bottom',
			duration: 3000,
			dismissOnPageChange: true
		});
		toast.present();
	}
	errorAlert(error) {
		const alert = this.alertController.create({
			title: 'Erreur !',
			subTitle: error.status,
			message: error.message,
			buttons: [
				{
					text:'OK',
					handler: () => {
							  //console.log('OK clicked');
							  this.navCtrl.setRoot(this.navCtrl.getActive().component);
							}
				}
			]
		});
		alert.present();
	}
	navTo(value) {
		this.navCtrl.push(value);
	}
}
